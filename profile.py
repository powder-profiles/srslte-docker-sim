import geni.portal as portal
import geni.rspec.igext as IG
import geni.rspec.pg as rspec


tourDescription = """

# srsLTE with simulated links via Docker

Use this profile to intantiate an end-to-end LTE network comprised of an EPC,
two eNBs, and two UEs using Docker and srsLTE release 20.04.1. The connections
between the UEs and eNBs are simulated (using ZMQ to pass baseband IQ samples).

See `docker-compose.yml` for more details about the topology.

"""

tourInstructions = """

After your experiment becomes ready, login to `hostnode` and do:

```
cd /local/repository
sudo docker-compose build
```

Verify that the build finishes without error, then do:

```
sudo docker-compose up
```

After the services are running and the UEs have assouciated with the eNBs, you
execute commands inside the various containers, e.g.,

```
sudo docker-compose exec ue1 ping 172.16.0.1
```

which will test the connectivity of `ue1` to `epc` via the TUN interface that
srsLTE creates. You can also start an interactive shell, e.g.,

```
sudo docker-compose exec ue2 /bin/bash
```

"""


class GLOBALS(object):
    HOST_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD"


pc = portal.Context()
pc.defineParameter("hardware_type", "node hardware type", portal.ParameterType.STRING,
                   "", longDescription="An optional hardware type for the host node.",
                   advanced=True)

params = pc.bindParameters()
pc.verifyParameters()
request = pc.makeRequestRSpec()

node = request.RawPC("hostnode")
if params.hardware_type:
    node.hardware_type = params.hardware_type

node.component_manager_id = "urn:publicid:IDN+emulab.net+authority+cm"
node.disk_image = GLOBALS.HOST_IMG
node.addService(rspec.Execute(shell="bash", command="/local/repository/startup.sh"))

tour = IG.Tour()
tour.Description(IG.Tour.MARKDOWN, tourDescription)
tour.Instructions(IG.Tour.MARKDOWN, tourInstructions)
request.addTour(tour)

pc.printRequestRSpec(request)
